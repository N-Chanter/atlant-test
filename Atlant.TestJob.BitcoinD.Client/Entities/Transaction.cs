﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Atlant.TestJob.BitcoinD.Client.Entities
{
    public class Transaction
    {
        [JsonProperty("account")]
        public string Account { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("category")]
        public string Category { get; set; }

        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }

        [JsonProperty("vout")]
        public uint Vout { get; set; }

        [JsonProperty("confirmations")]
        public uint Confirmations { get; set; }

        [JsonProperty("blockhash")]
        public string Blockhash { get; set; }

        [JsonProperty("blockindex")]
        public uint BlockIndex { get; set; }

        [JsonProperty("blocktime")]
        public long blocktime { get; set; }

        [Key]
        [JsonProperty("txid")]
        public string TxId { get; set; }

        [JsonProperty("time")]
        public long Time { get; set; }

        [JsonProperty("timereceived")]
        public long TimeReceived { get; set; }

        [JsonProperty("bip125-replaceable")]
        public string Bip125Replaceable { get; set; }

        [JsonProperty("finalized")]
        public bool Finalized { get; set; } = false;

        [JsonProperty("seen")]
        public bool Seen { get; set; } = false;
    }
}
