﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlant.TestJob.BitcoinD.Client
{
    /// <summary>
    /// Base object for RPC requests
    /// </summary>
    internal class RpcRequest
    {
        /// <summary>
        /// Protocol version
        /// </summary>
        [JsonProperty("jsonrpc")]
        public string Version { get; } = "1.0";

        /// <summary>
        /// Honestly I don't know what it is)))
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; } = "1";

        /// <summary>
        /// Content type specification
        /// </summary>
        [JsonProperty("method")]
        public string Method { get; } = "application/json-rpc";

        /// <summary>
        /// List of parameters transfered
        /// </summary>
        [JsonProperty("params")]
        public List<string> Parameters { get; }


        public RpcRequest(string methodName)
        {
            if (string.IsNullOrEmpty(methodName))
            {
                throw new Exception("RPC method name cannot be empty");
            }
            Parameters = new List<string>();
            Method = methodName;
        }

        /// <summary>
        /// Adds parameter range to request
        /// </summary>
        /// <param name="parameters">String parameters</param>
        public void AddParameters(params string[] parameters)
        {
            if (parameters == null) return;
            foreach(var parameter in parameters)
            {
                Parameters.Add(parameter);
            }
        }

    }
}
