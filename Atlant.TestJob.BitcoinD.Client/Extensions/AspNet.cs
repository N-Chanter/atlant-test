﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Atlant.TestJob.BitcoinD.Client.Extensions
{
    /// <summary>
    /// Extension class for easier connect to WebAPI / MVC applications
    /// </summary>
    public static class AspNet
    {
        /// <summary>
        /// Method should be used as an extension in ConfigureServices() method of Startup.cs
        /// </summary>
        /// <param name="services">IServiceCollection to extend</param>
        /// <param name="configuration">Service configuration as a parameter</param>
        public static void UseBitcoinDClient(this IServiceCollection services, Configuration configuration)
        {
            if (configuration == null)
            {
                configuration = new Configuration();
            }
            services.AddTransient<IBitcoindClient>(service => new BitcoindClient(configuration));
        }
    }
}
