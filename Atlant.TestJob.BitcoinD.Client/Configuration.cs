﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Atlant.TestJob.BitcoinD.Client
{
    public class Configuration
    {
        /// <summary>
        /// Base URL of the node
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Network username
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Network password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Node port
        /// </summary>
        public ushort Port { get; set; }

        /// <summary>
        /// Complete web address
        /// </summary>
        public string Address { get { return $"{Url}:{Port}"; } }
    }
}
