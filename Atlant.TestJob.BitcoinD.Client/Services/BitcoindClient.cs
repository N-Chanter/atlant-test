﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Atlant.TestJob.BitcoinD.Client
{
    internal class BitcoindClient: IBitcoindClient
    {
        internal Configuration _configuration;

        private const string SEND_TO_ADDRESS = "sendtoaddress";
        private const string LIST_TRANSACTIONS = "listtransactions";

        public BitcoindClient(Configuration config)
        {
            _configuration = config;
        }

        public object TestMethod(string procedureName, params string[] parameters)
        {
            return Request<object>(procedureName, parameters);
        }

        public Transaction Send(string address, double amount, string comment = "", string commentTo = "") // I know, but string.Empty doesn't work in parameters
        {
            return Request<Transaction>(SEND_TO_ADDRESS, address, amount.ToString(), comment, commentTo); 
        }

        public IList<Transaction> GetAll()
        {
            var result = Request<IList<Transaction>>(LIST_TRANSACTIONS);
            return result;
        }

        private string RpcRequest(string procedureName, params string[] parameters)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(new Uri(_configuration.Address));
            webRequest.Credentials = new NetworkCredential(_configuration.Username, _configuration.Password);
            webRequest.ContentType = "application/json-rpc";
            webRequest.Method = "POST";
            string responseValue = string.Empty;

            RpcRequest request = new RpcRequest(procedureName);
            request.AddParameters(parameters);

            string s = JsonConvert.SerializeObject(request, Formatting.Indented);
            byte[] byteArray = Encoding.UTF8.GetBytes(s);
            webRequest.ContentLength = byteArray.Length;
            Stream dataStream = webRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            try
            {
                StreamReader sReader = null;
                WebResponse webResponse = webRequest.GetResponse();
                sReader = new StreamReader(webResponse.GetResponseStream(), true);
                responseValue = sReader.ReadToEnd();
                //var data = JsonConvert.DeserializeObject(responseValue).ToString();
                return responseValue;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }            
        }

        private T Request<T>(string procedureName, params string[] parameters)
        {
            var rpcResponse = RpcRequest(procedureName, parameters);
            try
            {                
                var deserializedResponse = JsonConvert.DeserializeObject<JObject>(rpcResponse);
                var result = deserializedResponse["result"].ToObject<T>();
                if (result == null)
                {
                    throw new Exception($"Cannot convert RPC response to type {nameof(T)}");
                }
                return result;
            }
            catch(Exception ex)
            {
                var response = new
                {
                    exception = ex.Message,
                    procedure = procedureName,
                    response = rpcResponse
                };
                return (T)(object)response;
                
            }            
        }        
    }
}
