﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using System.Collections.Generic;

namespace Atlant.TestJob.BitcoinD.Client
{
    /// <summary>
    /// Client of Bitcoind web service
    /// </summary>
    public interface IBitcoindClient
    {
        /// <summary>
        /// Method for testing purposes
        /// </summary>
        /// <param name="procedureName">RPC procedure name</param>
        /// <param name="parameters">List of string parameters if any are present</param>
        /// <returns>JObject relevant to request</returns>
        object TestMethod(string procedureName, params string[] parameters);

        /// <summary>
        /// Send funds to specified address
        /// </summary>
        /// <param name="address">Recepient address</param>
        /// <param name="amount">Amount of transfered funds</param>
        /// <returns>Transaction ID in case of success. Null if failure</returns>
        Transaction Send(string address, double amount, string comment, string commentTo);

        /// <summary>
        /// Returns all transactions from bitcoind node
        /// </summary>
        /// <returns>Transaction array</returns>
        IList<Transaction> GetAll();
    }
}
