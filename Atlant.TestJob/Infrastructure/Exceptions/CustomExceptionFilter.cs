﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Atlant.TestJob.Infrastructure.Exceptions
{
    public class CustomExceptionFilterAttribute : Attribute, IExceptionFilter
    {

        public void OnException(ExceptionContext context)
        {
            context.ExceptionHandled = ApplicationFaultExceptionHandler(context);
        }


        private bool ApplicationFaultExceptionHandler(ExceptionContext context)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.PreconditionFailed;
            context.Result = new JsonResult(context.Exception); // TODO: Migrate to CustomException
            return true;
        }
    }
}
