﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Infrastructure.Exceptions
{
    // TODO
    public class CustomException
    {
        public string Message { get; set; }
        public StackTrace StackTrace { get; set; }
    }
}
