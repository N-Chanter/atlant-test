﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Data
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }

        public ApplicationDbContext(DbContextOptions options): base(options)
        {

        }
    }
}
