﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Data.Repositories.TransactionRepository
{
    public interface ITransactionRepository
    {
        IList<Transaction> GetAll();
        IList<Transaction> GetSent();
        IList<Transaction> GetReceived();
        bool SaveOrUpdateTransactions(IEnumerable<Transaction> transactions);
    }
}
