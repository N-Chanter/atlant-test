﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlant.TestJob.BitcoinD.Client.Entities;
using Microsoft.EntityFrameworkCore;

namespace Atlant.TestJob.Data.Repositories.TransactionRepository
{
    public class TransactionRepository : ITransactionRepository
    {
        private ApplicationDbContext _context;
        private DbSet<Transaction> Transactions { get { return _context.Transactions; } }

        public TransactionRepository(IServiceProvider serviceProvider, DbContextOptions options)
        {
            _context = new ApplicationDbContext(options);
        }

        public IList<Transaction> GetAll()
        {
            return Transactions.ToList();
        }

        public IList<Transaction> GetReceived()
        {
            return Transactions.Where(x => x.Category.Equals("receive")).ToList();
        }

        public IList<Transaction> GetSent()
        {
            return Transactions.Where(x => x.Category.Equals("send")).ToList();
        }

        public bool SaveOrUpdateTransactions(IEnumerable<Transaction> transactions)
        {
            try
            {
                foreach (var transaction in transactions)
                {
                    var hasChanges = false;
                    if (!Transactions.Any(x => x.TxId.Equals(transaction.TxId)))
                    {
                        var tx = Transactions.Find(transaction.TxId);
                        var equality = tx?.Equals(transaction);
                        Transactions.Add(transaction);
                    }
                    else if (transaction.Confirmations < 6)
                    {
                        Transactions.Attach(transaction);
                        Transactions.Update(transaction);
                    }
                }
                return _context.SaveChanges() > 0;
            }
            catch(Exception ex)
            {
                return false;
            }            
        }
    }
}
