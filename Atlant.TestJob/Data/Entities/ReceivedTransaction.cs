﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Data.Entities
{
    public class ReceivedTransaction
    {
        public string TxId { get; set; }
        public string FromAccount { get; set; }
    }
}
