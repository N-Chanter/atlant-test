﻿using Atlant.TestJob.BitcoinD.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Syncronizer
{
    /// <summary>
    /// Service for syncronization of db dataset with bitcoind actual data
    /// </summary>
    public interface ISyncronizer: IDisposable
    {
        /// <summary>
        /// Starts constant syncronization with bitcoind network
        /// </summary>
        /// <param name="interval"></param>
        void Start(TimeSpan interval);

        /// <summary>
        /// Stops syncronization execution
        /// </summary>
        /// <returns></returns>
        bool Stop();
    }
}
