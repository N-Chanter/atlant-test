﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using Microsoft.Extensions.DependencyInjection;
using Atlant.TestJob.Data.Repositories.TransactionRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Atlant.TestJob.Services.TransactionService;

namespace Atlant.TestJob.Syncronizer
{
    public class Syncronizer : ISyncronizer
    {        
        private readonly IServiceProvider _serviceProvider;
        private readonly IServiceScope _scope;
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionService _transactionService;

        private TimeSpan _interval;

        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;

        private int _errorCount = 0;

        public Syncronizer(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _scope = _serviceProvider.CreateScope();
            _transactionService = _scope?.ServiceProvider.GetRequiredService<ITransactionService>();
            _transactionRepository = _scope?.ServiceProvider.GetRequiredService<ITransactionRepository>();
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
        }

        public async void Start(TimeSpan interval)
        {
            _interval = interval;
            _errorCount = 0;

            // This should be highlighted as warning
            // because static analizer considers the task to be unwaited
            // but yet it is waited by Task.WaitAll() and we have guaranteed 
            // the result to be present in the next line
            Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    var task = UpdateDatabase();
                    Task.WaitAll(task);
                    var result = task.Result;

                    if (!result)
                    {
                        _errorCount++;
                    }

                    Thread.Sleep(interval);
                    if (_token.IsCancellationRequested)
                    {
                        break;
                    }
                }
            }, _token);
        }

        public bool Stop()
        {
            try
            {
                _tokenSource.Cancel();
                return true;
            }
            catch(Exception ex)
            {
                // TODO: Logging
                return false;
            }
        }

        private async Task<bool> UpdateDatabase()
        {
            var transactionsUpdateResult = await UpdateTransactions();
            var accountsUpdateResult = await UpdateAccounts();
            return transactionsUpdateResult && accountsUpdateResult;
        }

        private async Task<bool> UpdateTransactions()
        {
            var transactions = GetActualTransactions().Where(x => x.Confirmations < 6);
            return _transactionRepository.SaveOrUpdateTransactions(transactions);
        }

        private IList<Transaction> GetActualTransactions()
        {
            return _transactionService.GetActual();
        }

        private async Task<bool> UpdateAccounts()
        {
            // Coming soon

            // Here I have an uresolver question:
            // One of accounts in bitcoind always returns with empty string name "".
            // From the other side any attempt to send emty string as account name causes 500.
            // Will clarify this shortly and finish the method. If you can help and answer a few questions live (skype, any other messenger) it would be quick.
            return true;
        }

        public void Dispose()
        {
            // Coming soon
        }
    }
}
