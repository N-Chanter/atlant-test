﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Atlant.TestJob.Migrations
{
    public partial class Added_Seen_Column_To_Transaction : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Seen",
                table: "Transactions",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Seen",
                table: "Transactions");
        }
    }
}
