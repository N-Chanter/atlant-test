﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Atlant.TestJob.Migrations
{
    public partial class InnitialState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    Account = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    Amount = table.Column<double>(nullable: false),
                    Label = table.Column<string>(nullable: true),
                    Vout = table.Column<long>(nullable: false),
                    Confirmations = table.Column<long>(nullable: false),
                    Blockhash = table.Column<string>(nullable: true),
                    BlockIndex = table.Column<long>(nullable: false),
                    blocktime = table.Column<long>(nullable: false),
                    TxId = table.Column<string>(nullable: false),
                    Time = table.Column<long>(nullable: false),
                    TimeReceived = table.Column<long>(nullable: false),
                    Bip125Replaceable = table.Column<string>(nullable: true),
                    Finalized = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.TxId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Transactions");
        }
    }
}
