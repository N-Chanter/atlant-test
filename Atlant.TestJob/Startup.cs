﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Atlant.TestJob.BitcoinD.Client;
using Atlant.TestJob.Infrastructure.Exceptions;
using Atlant.TestJob.BitcoinD.Client.Extensions;
using Atlant.TestJob.Data;
using Microsoft.EntityFrameworkCore;
using Atlant.TestJob.Data.Repositories.TransactionRepository;
using Atlant.TestJob.Syncronizer;
using Atlant.TestJob.Services.TransactionService;

namespace Atlant.TestJob
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;

            var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)

            .AddEnvironmentVariables();

            Configuration = builder.Build();           
        }

        public IConfiguration Configuration { get; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            // Global exception handler (just comfortable)
            services.AddMvc(options =>
            {
                options.Filters.Add(new CustomExceptionFilterAttribute());
            });

            // Bicoind client (could use IOptions injection but this is a test task so didn't pitch perfect)
            services.UseBitcoinDClient(new Configuration {
                Url = "http://127.0.0.1",
                Port = 8332,
                Username = "testuser",
                Password = "testpassword"
            });

            // Database configuration
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // DI
            services.AddScoped<ITransactionRepository, TransactionRepository>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddSingleton<ISyncronizer, Syncronizer.Syncronizer>();
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            var syncronizer = provider.GetService<ISyncronizer>();
            syncronizer.Start(TimeSpan.FromSeconds(1));
        }
    }
}
