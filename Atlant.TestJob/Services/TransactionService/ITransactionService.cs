﻿using Atlant.TestJob.BitcoinD.Client.Entities;
using Atlant.TestJob.Services.TransactionService.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Services.TransactionService
{
    /// <summary>
    /// Transaction management service
    /// </summary>
    public interface ITransactionService
    {
        /// <summary>
        /// Sends specified amount of BTC to specified address
        /// </summary>
        /// <param name="address">Recepient address</param>
        /// <param name="amount">BTC amount to send</param>
        /// <returns>Transaction ID</returns>
        TransactionDto SendBtc(string address, double amount, string comment = null, string commentTo = null);

        /// <summary>
        /// Obtains last transactions.
        /// Last transaction is expected to be unrequested by this method
        /// or having less than 3 confirmations
        /// </summary>
        /// <returns>Array of transactions</returns>
        IList<TransactionDto> GetLast();
        
        /// <summary>
        /// Obtains all available transactions
        /// </summary>
        /// <returns>Array of transactions</returns>
        IList<TransactionDto> GetAll();

        /// <summary>
        /// Returns actual transactions as entities
        /// </summary>
        /// <returns>Array of transaction entities</returns>
        IList<Transaction> GetActual();
    }
}
