﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Atlant.TestJob.Services.TransactionService.DTO
{
    /// <summary>
    /// DTO for ITransactionService
    /// </summary>
    public class TransactionDto
    {
        /// <summary>
        /// Date of transaction
        /// </summary>
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        /// <summary>
        /// Recepient address
        /// </summary>
        [JsonProperty("address")]
        public string Address { get; set; }
        
        /// <summary>
        /// Amount of BTC transfered
        /// </summary>
        [JsonProperty("amount")]
        public double Amount { get; set; }

        /// <summary>
        /// Confirmations count
        /// </summary>
        [JsonProperty("confirmation")]
        public uint Confirmations { get; set; }
    }
}
