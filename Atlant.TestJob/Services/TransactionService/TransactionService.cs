﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlant.TestJob.Services.TransactionService.DTO;
using Atlant.TestJob.BitcoinD.Client;
using Atlant.TestJob.Data.Repositories.TransactionRepository;
using Atlant.TestJob.BitcoinD.Client.Entities;

namespace Atlant.TestJob.Services.TransactionService
{
    public class TransactionService : ITransactionService
    {
        private IBitcoindClient _client;
        private ITransactionRepository _transactionRepository;

        public TransactionService(IBitcoindClient client, ITransactionRepository transactionRepository)
        {
            _client = client;
            _transactionRepository = transactionRepository;
        }

        #region Public methods

        public IList<TransactionDto> GetAll()
        {
            return RangeToDto(_client.GetAll()).ToList();
        }

        public IList<TransactionDto> GetLast()
        {
            // Get all
            var result =  _client.GetAll().Where(x =>
                !x.Seen
                && x.Confirmations > 2
            );
            
            foreach(var transaction in result)
            {
                if (!transaction.Seen)
                {
                    transaction.Seen = true;
                    _transactionRepository.SaveOrUpdateTransactions(new Transaction[] { transaction });
                }
            }

            return RangeToDto(result).ToList();
        }

        // Only for ISyncronizer - DTO doesn't work there
        public IList<Transaction> GetActual()
        {
            return _client.GetAll();
        }

        public TransactionDto SendBtc(string address, double amount, string comment = null, string commentTo = null)
        {
            comment = comment ?? string.Empty;
            commentTo = commentTo ?? string.Empty;
            var tx = _client.Send(address, amount, comment, commentTo);
            return ToDto(tx);
        }

        #endregion Public Methods

        #region Private Methods

        private TransactionDto ToDto(Transaction transaction)
        {
            return new TransactionDto
            {
                Address = transaction.Address,
                Amount = transaction.Amount,
                Confirmations = transaction.Confirmations,
                Date = TimeStampToDateTime(transaction.Time)
            };
        }

        private IEnumerable<TransactionDto> RangeToDto(IEnumerable<Transaction> transactions)
        {
            foreach (var transaction in transactions)
            {
                yield return ToDto(transaction);
            }
        }

        private DateTime TimeStampToDateTime(long timestamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(timestamp);
            return dateTime;
        }

        #endregion Private Methods
    }
}
