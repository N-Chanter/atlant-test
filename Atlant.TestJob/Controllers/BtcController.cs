﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Atlant.TestJob.BitcoinD.Client;
using Atlant.TestJob.Services.TransactionService;
using Atlant.TestJob.Services.TransactionService.DTO;

namespace Atlant.TestJob.Controllers
{
    [Produces("application/json")]
    [Route("api/btc")]
    public class BtcController : Controller
    {
        private IBitcoindClient _client;
        private ITransactionService _transactionService;

        public BtcController(IBitcoindClient client, ITransactionService transactionService)
        {
            _client = client;
            _transactionService = transactionService;
        }

        [HttpPost]
        [Route("send")]
        [ProducesResponseType(typeof(TransactionDto), 200)]
        public IActionResult SendBtc(string address, double amount, string comment, string commentTo)
        {
            return Json(_transactionService.SendBtc(address, amount, comment, commentTo));
        }

        [HttpGet]
        [Route("txs")]
        [ProducesResponseType(typeof(IList<TransactionDto>), 200)]
        public IActionResult GetLast()
        {
            return Json(_transactionService.GetLast());
        }


    }
}
